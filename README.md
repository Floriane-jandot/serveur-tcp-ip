Implémentation d'un petit serveur TCP-IP entre une bibliothèque et ses clients

Créé en 2022 avec deux collaboratrices, développé en C

Le serveur récupère les informations à partir d'un fichier texte contenant des informations de livres.
Il crée deux sockets qui permettent de communiquer avec un ou plusieurs clients : lorsque le serveur accepte une connexion client, il crée un fils qui échangera les informations souhaitées via des _pipes_. 
